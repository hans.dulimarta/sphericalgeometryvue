module.exports = {
    icons: {
      boundaryCircle: {
        strokeWidth: 1.5,
        color: "hsla(0, 0%, 0%, 1)"
      },
      emphasize: {
        angleMarker: {
          strokeWidth: {
            front: 2.5,
            back: 2.5
          },
          edgeColor: {
            front: "hsla(0, 0%, 0%, 1)",
            back: "hsla(0, 0%, 0%, 0.3)"
          },
          fillColor: {
            front: "hsla(254, 100%, 90%, 1)",
            back: "hsla(10,100%, 50%, 1)"
          }
        },
        circle: {
          strokeWidth: {
            front: 1,
            back: 1
          },
          edgeColor: {
            front: "hsla(0, 0%, 0%, 1)",
            back: "hsla(0, 0%, 0%, 1)"
          },
          fillColor: {
            front: "hsla(0, 100%, 75%, 1)",
            back: "hsla(0, 100%, 75%, 1)"
          }
        },
        ellipse: {
          strokeWidth: {
            front: 1,
            back: 1
          },
          edgeColor: {
            front: "hsla(0, 0%, 0%, 1)",
            back: "hsla(0, 0%, 0%, 1)"
          },
          fillColor: {
            front: "hsla(0, 100%, 75%, 1)",
            back: "hsla(0, 100%, 75%, 1)"
          }
        },
        point: {
          strokeWidth: {
            front: 0.7,
            back: 0.7
          },
          edgeColor: {
            front: "hsla(0, 0%, 0%, 1)",
            back: "hsla(0, 0%, 0%, 1)"
          },
          fillColor: {
            front: "hsla(0, 100%, 75%, 1)",
            back: "hsla(0, 100%, 75%, 1)"
          }
        },
        line: {
          strokeWidth: {
            front: 1.5,
            back: 1.5
          },
          edgeColor: {
            front: "hsla(217, 90%, 61%, 1)",
            back: "hsla(217, 90%, 80%, 1)"
          }
        },
        segment: {
          strokeWidth: {
            front: 1.5,
            back: 1.5
          },
          edgeColor: {
            front: "hsla(217, 90%, 61%, 1)",
            back: "hsla(217, 90%, 80%, 1)"
          }
        }
      },
      angleMarker: {
        scale: {
          front: 7,
          back: 5
        },
        strokeWidth: {
          front: 1,
          back: 1
        },
        edgeColor: {
          front: "hsla(0, 0%, 40%, 1)",
          back: "hsla(0, 0%, 60%, 1)"
        },
        fillColor: {
          front: "hsla(0, 0%, 90%, 0.4)",
          back: "hsla(0, 0%, 100%, 0.2)"
        }
      },
      circle: {
        strokeWidth: {
          front: 1,
          back: 1
        },
        edgeColor: {
          front: "hsla(0, 0%, 40%, 1)",
          back: "hsla(0, 0%, 60%, 1)"
        },
        fillColor: {
          front: "hsla(0, 0%, 90%, 0.4)",
          back: "hsla(0, 0%, 100%, 0.2)"
        }
      },
      ellipse: {
        strokeWidth: {
          front: 1,
          back: 1
        },
        edgeColor: {
          front: "hsla(0, 0%, 40%, 1)",
          back: "hsla(0, 0%, 60%, 1)"
        },
        fillColor: {
          front: "hsla(0, 0%, 90%, 0.4)",
          back: "hsla(0, 0%, 100%, 0.2)"
        }
      },
      point: {
        scale: {
          front: 7,
          back: 9
        },
        strokeWidth: {
          front: 0.8,
          back: 0.7
        },
        edgeColor: {
          front: "hsla(0, 0%, 40%, 1)",
          back: "hsla(0, 0%, 60%, 1)"
        },
        fillColor: {
          front: "hsla(0, 0%, 90%, 1)",
          back: "hsla(0, 0%, 100%, 1)"
        }
      },
      line: {
        strokeWidth: {
          front: 1,
          back: 1
        },
        edgeColor: {
          front: "hsla(0, 0%, 40%, 1)",
          back: "hsla(0, 0%, 60%, 1)"
        }
      },
      segment: {
        strokeWidth: {
          front: 1,
          back: 1
        },
        edgeColor: {
          front: "hsla(0, 0%, 40%, 1)",
          back: "hsla(0, 0%, 60%, 1)"
        }
      }
    }
};
